# d-slug
Sluglifies strings to be used in URLs or file paths

```d
import slug;

void main() {
  assert("hello there".slug == "hello-there");
  assert("here we are".slug == "here-we-are");
  assert("what-the-hell-is-this".slug);
  assert("do-i-work-properly".slug);
}
```

## License
This code is licensed under the MIT license. For more information please refer
to the [LICENSE](/LICENSE) file.

## Donations
Would you like to buy me a beer? Send bitcoin to 3JjxJydvoJjTrhLL86LGMc8cNB16pTAF3y

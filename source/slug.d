import std.regex : ctRegex, replaceAll;
import std.string : toLower;

auto slugR = ctRegex!(r"[^a-z]+", "g");

string slug(string target) {
  auto ret = target.toLower.replaceAll(slugR, "-");

  if(ret[0] == '-') {
    ret = ret[1..$];
  }

  if(ret[$-1] == '-') {
    ret = ret[0..$-1];
  }

  return ret;
}

unittest {
  assert("hello there".slug == "hello-there");
  assert("here we are".slug == "here-we-are");
  assert("what-the-hell-is-this".slug);
  assert("do-i-work-properly".slug);
}
